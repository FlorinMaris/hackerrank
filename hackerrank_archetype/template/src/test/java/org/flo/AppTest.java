package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={" paste input values here "};
        String expectedOutput=" paste expected output here ";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
