package org.flo;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

class Solution {


    public static String main(String[] args) {
        // setting up input and output
        ByteArrayInputStream in= new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out= new ByteArrayOutputStream();
        PrintStream printStream=new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int[] array= new int[n];
        for (int i=0;i<n;i++){
            array[i]=sc.nextInt();
        }
        int sumCount=0;
        for(int subArrayLength=1;subArrayLength<=n;subArrayLength++){
            for (int i=0;i<=n-subArrayLength;i++){
                if(isNegativeSum(subArrayLength,i,array)){
                    sumCount++;
                }
            }
        }
        System.out.println(sumCount);
        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }
    public static boolean isNegativeSum(int arrayLength,int currentIndex, int[] a){
        int sum=0;
        for(int i=currentIndex;i<currentIndex+arrayLength;i++){
            sum+=a[i];
        }
        return sum<0;
    }
}