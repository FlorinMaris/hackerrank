package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/java-negative-subarray/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"5\n" +
                "1 -2 4 -5 1"};
        String expectedOutput="9\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
