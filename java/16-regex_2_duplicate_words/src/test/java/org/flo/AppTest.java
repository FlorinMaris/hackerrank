package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/duplicate-word/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"5\n" +
                "Goodbye bye bye world world world\n" +
                "Sam went went to to to his business\n" +
                "Reya is is the the best player in eye eye game\n" +
                "in inthe\n" +
                "Hello hello Ab aB"};
        String expectedOutput="Goodbye bye world\n" +
                "Sam went to his business\n" +
                "Reya is the best player in eye game\n" +
                "in inthe\n" +
                "Hello Ab\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
