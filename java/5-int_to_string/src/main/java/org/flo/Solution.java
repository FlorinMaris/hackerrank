    package org.flo;


    import java.io.ByteArrayInputStream;
    import java.io.ByteArrayOutputStream;
    import java.io.PrintStream;
    import java.util.*;

    class Solution {


        public static String main(String[] args) {
            // setting up input and output
            ByteArrayInputStream in = new ByteArrayInputStream(args[0].getBytes());
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            PrintStream printStream = new PrintStream(out);
            System.setIn(in);
            System.setOut(printStream);
            // code from HackerRank goes here
            Scanner scanner = new Scanner(System.in);
            int n = scanner.nextInt();
            scanner.close();
            //String s=???; Complete this line below
            Integer number=n;
            String s=number.toString();
            if (n == Integer.parseInt(s)) {
                System.out.println("Good job");
            } else {
                System.out.println("Wrong answer.");
            }
            // returning output in order to apply unit tests
            printStream.close();
            return out.toString();
        }
    }