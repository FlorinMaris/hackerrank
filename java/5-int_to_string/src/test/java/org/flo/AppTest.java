package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/java-int-to-string/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"100"};
        String expectedOutput="Good job\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
