package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/java-anagrams/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"anagram\n" + "margana "};
        String expectedOutput="Anagrams\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }

    @Test
    public void testCase2(){
        String[] input={"anagra\n" + "marana "};
        String expectedOutput="Not Anagrams\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
    @Test
    public void testCase3(){
        String[] input={"Hello\n" + "hello"};
        String expectedOutput="Anagrams\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
    @Test
    public void testDifferentLengths(){
        String[] input={"aa\n" + "A"};
        String expectedOutput="Not Anagrams\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
