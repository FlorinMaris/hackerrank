package org.flo;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;


class Solution {

    static boolean isAnagram(String a, String b){
        a=a.toLowerCase();
        b=b.toLowerCase();
        List<Character> aChars=toCharList(a);
        List<Character> bChars=toCharList(b);
        Collections.sort(aChars);
        Collections.sort(bChars);
        if(aChars.size()!=bChars.size()) return false;
        for (int i=0;i<aChars.size();i++) {
            if(aChars.get(i).compareTo(bChars.get(i))!=0) return false;
        }
        return true;

    }
    static List<Character> toCharList(String string){
        List<Character> list=new ArrayList<Character>();
        for (int i=0;i<string.length();i++) {
            list.add(string.charAt(i));
        }
        Collections.sort(list);
        return list;
    }
    public static String main(String[] args) {
        // setting up input and output
        ByteArrayInputStream in= new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out= new ByteArrayOutputStream();
        PrintStream printStream=new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here
        Scanner scan = new Scanner(System.in);
        String a = scan.next();
        String b = scan.next();
        scan.close();
        boolean ret = isAnagram(a, b);
        System.out.println( (ret) ? "Anagrams" : "Not Anagrams" );
        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }
}