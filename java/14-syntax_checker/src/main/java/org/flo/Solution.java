package org.flo;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

class Solution {


    public static String main(String[] args) {
        // setting up input and output
        ByteArrayInputStream in = new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here
        Scanner scanner = new Scanner(System.in);
        int testCases = Integer.parseInt(scanner.nextLine());
        while (testCases > 0) {
            String pattern = scanner.nextLine();
            try {
                Pattern compiledPattern = Pattern.compile(pattern);
                System.out.println("Valid");
            } catch (PatternSyntaxException patternException) {
                System.out.println("Invalid");
            }
            testCases--;
        }
        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }
}