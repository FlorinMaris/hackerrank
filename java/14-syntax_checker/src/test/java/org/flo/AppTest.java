package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/pattern-syntax-checker/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"3\n" +
                "([A-Z])(.+)\n" +
                "[AZ[a-z](a-z)\n" +
                "batcatpat(nat"};
        String expectedOutput="Valid\n" +
                "Invalid\n" +
                "Invalid\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
