package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/sha-256/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"HelloWorld"};
        String expectedOutput="872e4e50ce9990d8b041330c47c9ddd11bec6b503ae9386a99da8584e9bb12c4";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
    @Test
    public void testCase2(){
        String[] input={"Javarmi123"};
        String expectedOutput="f1d5f8d75bb55c777207c251d07d9091dc10fe7d6682db869106aacb4b7df678";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
