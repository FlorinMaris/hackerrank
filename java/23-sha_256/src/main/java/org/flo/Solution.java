package org.flo;


import sun.nio.cs.StandardCharsets;

import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

class Solution {


    public static String main(String[] args) {
        // setting up input and output
        ByteArrayInputStream in= new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out= new ByteArrayOutputStream();
        PrintStream printStream=new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here
        Scanner scanner=new Scanner(System.in);
        String message=scanner.nextLine();
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            byte[] encoded=messageDigest.digest(message.getBytes());
            String result= DatatypeConverter.printHexBinary(encoded).toLowerCase();
            System.out.print(result);
        }catch (NoSuchAlgorithmException e){
            System.out.println("No SHA-256 algorithm found");
        }
        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }

}