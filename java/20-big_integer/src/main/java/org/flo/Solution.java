package org.flo;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigInteger;
import java.util.*;

class Solution {


    public static String main(String[] args) {
        // setting up input and output
        ByteArrayInputStream in= new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out= new ByteArrayOutputStream();
        PrintStream printStream=new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here
        Scanner sc= new Scanner(System.in);
        BigInteger first=sc.nextBigInteger();
        BigInteger second=sc.nextBigInteger();
        BigInteger sum=first.add(second);
        BigInteger product=first.multiply(second);
        System.out.print(sum+"\n"+product);
        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }
}