package org.flo;


import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/java-static-initializer-block/problem
 */
public class Test1
{
    @Test
    public void testCase1(){
        String expectedOutput="3";
        Input.setInput("1\n" +
                "3");
        Solution.main(new String[0]);
        Assert.assertEquals(expectedOutput,Output.getOutput());
    }
}
