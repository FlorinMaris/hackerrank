package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/java-static-initializer-block/problem
 */
public class Test2
{

    @Test
    public void testCase2(){
        new Input();
        Input.setInput("-1\n" + "2");
        new Solution();
        String expectedOutput="java.lang.Exception: Breadth and height must be positive";
        Solution.main(new String[0]);
        Assert.assertEquals(expectedOutput,Output.getOutput());
    }
}
