package org.flo;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

class Solution {
    static boolean flag;
    static int B;
    static int H;
    static ByteArrayInputStream in;
    static ByteArrayOutputStream out;
    static {
        in= new ByteArrayInputStream(Input.getInput().getBytes());
        out= new ByteArrayOutputStream();
        PrintStream printStream=new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);

        Scanner scanner= new Scanner(System.in);
        B=scanner.nextInt();
        H=scanner.nextInt();
        try{
        flag=initialize(B,H);
            }catch (Exception e){
            System.out.print(e);
            Output.setOutput(out.toString());
        }


    }
    static boolean initialize(int breadth,int height) throws Exception{
        if(breadth<=0 || height <=0) throw new Exception("Breadth and height must be positive");
        return true;
    }

    public static void main(String[] args) {
        // setting up input and output
        // code from HackerRank goes here
        if(flag){
            int area=B*H;
            System.out.print(area);
            Output.setOutput(out.toString());
        }
    }
}