package org.flo;

public class Input {
    private static String input;

    public static String getInput() {
        return input;
    }

    public static void setInput(String input) {
        Input.input = input;
    }
}
