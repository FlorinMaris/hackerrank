package org.flo;

public class Output {
   private static String output;

    public static String getOutput() {
        return output;
    }

    public static void setOutput(String output) {
        Output.output = output;
    }
}
