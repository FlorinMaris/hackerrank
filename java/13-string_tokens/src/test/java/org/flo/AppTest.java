package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/java-string-tokens/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"He is a very very good boy, isn't he?"};
        String expectedOutput="10\n" +
                "He\n" +
                "is\n" +
                "a\n" +
                "very\n" +
                "very\n" +
                "good\n" +
                "boy\n" +
                "isn\n" +
                "t\n" +
                "he\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
    @Test
    public void testSingleChar(){
        String[] input={"a"};
        String expectedOutput="1\n" +
                "a\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }@Test
    public void testLeadingSpaces(){
        String[] input={"           YES      leading spaces        are valid,    problemsetters are         evillllll"};
        String expectedOutput="8\n" +
                "YES\n" +
                "leading\n" +
                "spaces\n" +
                "are\n" +
                "valid\n" +
                "problemsetters\n" +
                "are\n" +
                "evillllll\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }

}
