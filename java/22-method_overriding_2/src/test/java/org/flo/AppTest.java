package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={};
        String expectedOutput="Hello I am a motorcycle, I am a cycle with an engine.\n" +
                "My ancestor is a cycle who is a vehicle with pedals.\n";
        Assert.assertEquals(expectedOutput, Solution.main(input));
    }
}
