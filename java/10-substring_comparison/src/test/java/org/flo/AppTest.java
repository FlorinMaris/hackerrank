package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/java-string-compare/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"welcometojava\n" + "3"};
        String expectedOutput="ava\n" + "wel\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}

