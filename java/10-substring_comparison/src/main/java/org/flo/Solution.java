package org.flo;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

class Solution {


    public static String main(String[] args) {
        // setting up input and output
        ByteArrayInputStream in= new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out= new ByteArrayOutputStream();
        PrintStream printStream=new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here

        Scanner scan = new Scanner(System.in);
        String s = scan.next();
        int k = scan.nextInt();
        scan.close();
        System.out.println(getSmallestAndLargest(s, k));

        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }

    public static String getSmallestAndLargest(String s, int k) {
        String smallest = "";
        String largest = "";
        String aux;
        smallest=s.substring(0,k);
        for (int i=0;i<=s.length()-k;i++){
            aux=s.substring(i,i+k);
            smallest=smallest.compareTo(aux)>0?aux:smallest;
            largest=largest.compareTo(aux)<0?aux:largest;
        }
        return smallest + "\n" + largest;
    }
}