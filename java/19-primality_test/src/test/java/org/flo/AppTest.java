package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/java-primality-test/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"13"};
        String expectedOutput="prime\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
    @Test
    public void testCase2(){
        String[] input={"33"};
        String expectedOutput="not prime\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
