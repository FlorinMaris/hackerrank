package org.flo;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.*;

class Solution {


    public static String main(String[] args) {
        // setting up input and output
        ByteArrayInputStream in= new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out= new ByteArrayOutputStream();
        PrintStream printStream=new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here
        Scanner sc= new Scanner(System.in);
        int n=sc.nextInt();
        String []s=new String[n+2];
        for(int i=0;i<n;i++){
            s[i]=sc.next();
        }
        sc.close();
        //
        for(int i=0;i<n-1;i++){
            for(int j=1;j<n;j++){
                BigDecimal current=new BigDecimal(s[j]);
                BigDecimal previous=new BigDecimal(s[j-1]);
                if (current.compareTo(previous)>0){
                 s[n]=s[j];
                 s[j]=s[j-1];
                 s[j-1]=s[n];
                }
            }
        }
        //
        for(int i=0;i<n;i++)
        {
            System.out.println(s[i]);
        }
        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }
}