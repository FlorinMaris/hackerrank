package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/java-bigdecimal/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"9\n" +
                "-100\n" +
                "50\n" +
                "0\n" +
                "56.6\n" +
                "90\n" +
                "0.12\n" +
                ".12\n" +
                "02.34\n" +
                "000.000"};
        String expectedOutput="90\n" +
                "56.6\n" +
                "50\n" +
                "02.34\n" +
                "0.12\n" +
                ".12\n" +
                "0\n" +
                "000.000\n" +
                "-100\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
    @Test
    public void testCase2(){
        String[] input={"5\n"+
                "-100\n"+
                "0\n" +
                "0.12\n" +
                ".12\n" +
                "000.000"};
        String expectedOutput=
                "0.12\n" +
                ".12\n" +
                "0\n" +
                "000.000\n"+
                "-100\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
