package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/java-end-of-file/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"Hello world\n" +
                "I am a file\n" +
                "Read me until end-of-file."};
        String expectedOutput="1 Hello world\n" +
                "2 I am a file\n" +
                "3 Read me until end-of-file.\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
