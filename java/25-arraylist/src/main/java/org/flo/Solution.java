package org.flo;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

class Solution {


    public static String main(String[] args) {
        // setting up input and output
        ByteArrayInputStream in= new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out= new ByteArrayOutputStream();
        PrintStream printStream=new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here
        Scanner sc = new Scanner(System.in);
        int n=sc.nextInt();
        List<List> list=new ArrayList<List>();
        for (int i=0;i<n;i++){
            int subListLength=sc.nextInt();
            list.add(readIntegerList(subListLength,sc));
        }
        n=sc.nextInt();
        List<List> requests=new ArrayList<List>();
        for (int i=0;i<n;i++){
            requests.add(readIntegerList(2,sc));
        }
        for (List<Integer> request:requests){
            if(isInRange(request.get(0),list)){
                if( isInRange(request.get(1),list.get(request.get(0)-1))){
                    System.out.println(list.get(request.get(0)-1).get(request.get(1)-1));
                }
                else{
                    System.out.println("ERROR!");
                }
            }
            else{
                System.out.println("ERROR!");
            }

        }

        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }
    public static List<Integer> readIntegerList(int n, Scanner sc){
       List<Integer> list=new ArrayList<Integer>();
       for(int i=0;i<n;i++){
           list.add(sc.nextInt());
       }
       return list;
    }
    public static boolean isInRange(int index,List list){
        return index-1<list.size();
    }
}