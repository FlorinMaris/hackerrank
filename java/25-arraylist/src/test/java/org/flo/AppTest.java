package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/java-arraylist/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"5\n" +
                "5 41 77 74 22 44\n" +
                "1 12\n" +
                "4 37 34 36 52\n" +
                "0\n" +
                "3 20 22 33\n" +
                "5\n" +
                "1 3\n" +
                "3 4\n" +
                "3 1\n" +
                "4 3\n" +
                "5 5"};
        String expectedOutput="74\n" +
                "52\n" +
                "37\n" +
                "ERROR!\n" +
                "ERROR!\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
