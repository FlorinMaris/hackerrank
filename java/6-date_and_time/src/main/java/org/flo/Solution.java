package org.flo;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.*;

class Solution {


    public static String main(String[] args) {
        // setting up input and output
        ByteArrayInputStream in = new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here
        Scanner scanner = new Scanner(System.in);
        String month = scanner.next();
        String day = scanner.next();
        String year = scanner.next();

        System.out.println(getDay(day, month, year));

        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }
    public static String getDay(String day, String month, String year) {
        LocalDate date=LocalDate.of(Integer.parseInt(year),Integer.parseInt(month),Integer.parseInt(day));
        return date.getDayOfWeek().toString() ;
    }
}