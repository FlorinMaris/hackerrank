package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/java-date-and-time/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"08 05 2015"};
        String expectedOutput="WEDNESDAY\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
