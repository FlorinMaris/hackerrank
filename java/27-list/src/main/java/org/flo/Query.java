package org.flo;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Query {
    private List<Integer> list ;
    enum Operations {Insert, Delete}
    private Operations operation;
    private Scanner scanner;
    private int valueToReplace;
    private int index;

    Query(List<Integer> list, Scanner scanner) throws Exception {
        this.list = list;
        this.scanner = scanner;
        scanner.nextLine();
        String op = scanner.nextLine();
        if (op.equals(Operations.Insert.name())) {
            this.operation = Operations.Insert;
        } else {
            if (op.equals(Operations.Delete.name())) {
                this.operation = Operations.Delete;
            }else {
                throw new Exception("Operation could not be found");
            }
        }
        this.index= scanner.nextInt();
        if (operation == Operations.Insert) {
            this.valueToReplace=scanner.nextInt();
        }

    }

    public List<Integer> getProcessedList(){
        if (this.operation==Operations.Insert){
            this.list.add(index,valueToReplace);
        }else{
            list.remove(index);
        }
        return this.list;
    }

}
