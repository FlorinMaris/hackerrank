package org.flo;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

class Solution {


    public static String main(String[] args) {
        // setting up input and output
        ByteArrayInputStream in= new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out= new ByteArrayOutputStream();
        PrintStream printStream=new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here
        Scanner scanner=new Scanner(System.in);
        int n= scanner.nextInt();
        List<Integer> list=new LinkedList<Integer>();
        for (int i=0;i<n;i++){
            list.add(scanner.nextInt());
        }
        n=scanner.nextInt();
        for (int i=0;i<n;i++){
            try {
                Query query = new Query(list, scanner);
                list=query.getProcessedList();
            }catch (Exception e){
                System.out.println(e.getMessage());
            }
        }
        for (Integer number:list){
            System.out.print(number+" ");
        }
        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }
}