package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/java-list/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"5\n" +
                "12 0 1 78 12\n" +
                "2\n" +
                "Insert\n" +
                "5 23\n" +
                "Delete\n" +
                "0"};
        String expectedOutput="0 1 78 12 23 ";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
