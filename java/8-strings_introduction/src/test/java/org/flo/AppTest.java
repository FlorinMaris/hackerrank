package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/java-strings-introduction/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"hello\n" + "java"};
        String expectedOutput="9\n" + "No\n" + "Hello Java\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }

    @Test
    public void testCase2(){
        String[] input={"hello\n" + "hello"};
        String expectedOutput="10\n" + "No\n" + "Hello Hello\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
