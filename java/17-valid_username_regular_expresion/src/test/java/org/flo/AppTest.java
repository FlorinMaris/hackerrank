package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/valid-username-checker/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"8\n" +
                "Julia\n" +
                "Samantha\n" +
                "Samantha_21\n" +
                "1Samantha\n" +
                "Samantha?10_2A\n" +
                "JuliaZ007\n" +
                "Julia@007\n" +
                "_Julia007"};
        String expectedOutput="Invalid\n" +
                "Valid\n" +
                "Valid\n" +
                "Invalid\n" +
                "Invalid\n" +
                "Valid\n" +
                "Invalid\n" +
                "Invalid\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
