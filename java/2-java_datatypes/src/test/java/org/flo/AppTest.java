package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/java-datatypes/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={
                "5\n" +
                "-150\n" +
                "150000\n" +
                "1500000000\n" +
                "213333333333333333333333333333333333\n" +
                "-100000000000000"
        };
        String expectedOutput="-150 can be fitted in:\n" +
                "* short\n" +
                "* int\n" +
                "* long\n" +
                "150000 can be fitted in:\n" +
                "* int\n" +
                "* long\n" +
                "1500000000 can be fitted in:\n" +
                "* int\n" +
                "* long\n" +
                "213333333333333333333333333333333333 can't be fitted anywhere.\n" +
                "-100000000000000 can be fitted in:\n" +
                "* long\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
