package org.flo;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Solution {


    public static String main(String[] args) {
        // setting up input and output
        ByteArrayInputStream in= new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out= new ByteArrayOutputStream();
        PrintStream printStream=new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here
        Scanner scanner = new Scanner(System.in);
        int testCases = Integer.parseInt(scanner.nextLine());
        while(testCases>0){
            String line = scanner.nextLine();
            Pattern tags =Pattern.compile("<(.+)>([^<]+)</\\1>");
            Matcher matcher=tags.matcher(line);
            boolean foundTag=false;
            while (matcher.find()){
                System.out.println(matcher.group(2));
                foundTag=true;
            }
            if(!foundTag){
                System.out.println("None");
            }
            testCases--;
        }
        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }

}