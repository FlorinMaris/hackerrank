package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/java-1d-array/problem
 */
public class AppTest {
    @Test
    public void testCase1() {
        String[] input = {"4\n" +
                "5 3\n" +
                "0 0 0 0 0\n" +
                "6 5\n" +
                "0 0 0 1 1 1\n" +
                "6 3\n" +
                "0 0 1 1 1 0\n" +
                "3 1\n" +
                "0 1 0"};
        String expectedOutput = "YES\n" +
                "YES\n" +
                "NO\n" +
                "NO\n";
        Assert.assertEquals(expectedOutput, Solution.main(input));
    }

    @Test
    public void testCase2() {
        String[] input = {"3\n" +
                "6 2\n" +
                "0 1 0 1 0 1\n" +
                "10 6\n" +
                "0 0 1 1 0 0 1 1 0 0\n" +
                "10 3\n" +
                "0 0 1 1 0 0 1 1 0 0"};
        String expectedOutput = "YES\n" +
                "NO\n" +
                "YES\n";
        Assert.assertEquals(expectedOutput, Solution.main(input));
    }
}