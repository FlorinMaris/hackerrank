package org.flo;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

class Solution {

    private static boolean canWin(int leap, int[] game) {
        return playGame(0,game,leap);
    }
    private static boolean playGame(int currentIndex, int[] game, int leap){
      if(isWon(currentIndex,game)){
          return true;
      }
      if(isLost(currentIndex,game)){
          return false;
      }
      game[currentIndex]=1;
        return (playGame(currentIndex-1,game,leap) ||
                playGame(currentIndex+1,game,leap) || playGame(currentIndex+leap,game,leap) );
    }
    private static boolean isWon(int currentIndex,int[] game){
        return currentIndex>game.length-1 ;
    }
    private static boolean isLost(int currentIndex,int[] game){
        return currentIndex<0 || game[currentIndex]==1 ;
    }

    public static String main(String[] args) {
        // setting up input and output
        ByteArrayInputStream in= new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out= new ByteArrayOutputStream();
        PrintStream printStream=new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here
        Scanner scan = new Scanner(System.in);
        int q = scan.nextInt();
        while (q-- > 0) {
            int n = scan.nextInt();
            int leap = scan.nextInt();

            int[] game = new int[n];
            for (int i = 0; i < n; i++) {
                game[i] = scan.nextInt();
            }

            System.out.println( (canWin(leap, game)) ? "YES" : "NO" );
        }
        scan.close();
        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }
}