package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/java-regex/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"000.12.12.034\n" +
                "121.234.12.12\n" +
                "23.45.12.56\n" +
                "00.12.123.123123.123\n" +
                "122.23\n" +
                "Hello.IP"};
        String expectedOutput="true\n" +
                "true\n" +
                "true\n" +
                "false\n" +
                "false\n" +
                "false\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
