package org.flo;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.text.NumberFormat;
import java.util.*;

class Solution {


    public static String main(String[] args) {
        // setting up input and output
        ByteArrayInputStream in= new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out= new ByteArrayOutputStream();
        PrintStream printStream=new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here
        Scanner scanner = new Scanner(System.in);
        double payment = scanner.nextDouble();
        scanner.close();

        // Write your code here.
        NumberFormat formatPayment=NumberFormat.getCurrencyInstance(Locale.US);
        String us=formatPayment.format(payment);
        formatPayment=NumberFormat.getCurrencyInstance(Locale.CHINA);
        String china=formatPayment.format(payment);
        formatPayment=NumberFormat.getCurrencyInstance(Locale.FRANCE);
        String france=formatPayment.format(payment);
        final Locale INDIA=new Locale("en","IN");
        formatPayment=NumberFormat.getCurrencyInstance(INDIA);
        String india=formatPayment.format(payment);
        System.out.println("US: " + us);
        System.out.println("India: " + india);
        System.out.println("China: " + china);
        System.out.println("France: " + france);

        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }
}