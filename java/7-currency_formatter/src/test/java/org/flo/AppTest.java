package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/java-currency-formatter/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"12324.134"};
        String expectedOutput="US: $12,324.13\n" +
                "India: Rs.12,324.13\n" +
                "China: ￥12,324.13\n"+
                "France: 12 324,13 €\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
