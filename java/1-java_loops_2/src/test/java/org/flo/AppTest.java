package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/java-loops/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"2\n" + "0 2 10\n" + "5 3 5"};
        String expectedOutput="2 6 14 30 62 126 254 510 1022 2046 \n" + "8 14 26 50 98 \n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
