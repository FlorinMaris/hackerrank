package org.flo;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

class Solution {


    public static String main(String[] args) {
        // setting up input and output
        ByteArrayInputStream in= new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out= new ByteArrayOutputStream();
        PrintStream printStream=new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here
        Scanner sc = new Scanner(System.in);
        int t=sc.nextInt();
        for(int i=0;i<t;i++){
            int a = sc.nextInt();
            int b = sc.nextInt();
            int n = sc.nextInt();
            System.out.println(calculateSum(a,b,n));

        }
        sc.close();

        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }
    public static String calculateSum(int a,int b,int n){
        int sum=0;
        String result="";
        for (int i=0;i<n;i++){
            sum=a;
            for (int j=0;j<=i;j++){
            sum+=b*Math.pow(2,j);
            }
            result+=sum+" ";
        }
        return result;
    }
}