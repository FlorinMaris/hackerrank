package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/java-substring/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"Helloworld\n" + "3 7"};
        String expectedOutput="lowo\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
