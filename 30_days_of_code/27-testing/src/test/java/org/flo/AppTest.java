package org.flo;


import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


/**
 * The text of the task can be found at the following link
 * <p>
 * https://www.hackerrank.com/challenges/30-testing/problem
 */
public class AppTest {
    @Test
    public void testCase1() {
        String[] input = {"2\n" + "4 3\n" + "-1 -3 4 2\n" + "4 2\n" + "0 -1 2 1"};
        String expectedOutput = "YES\n" + "NO\n";
        Assert.assertEquals(expectedOutput, Solution.main(input));
    }

    @Test
    public void testCase2() {
        String[] input = {"1\n" + "20 12\n" + "-4 -6 4 2 0 0 0 12 14 15 16 -5 -6 1 0 -1 0 -2 3 4\n"};
        String expectedOutput = "YES\n";
        Assert.assertEquals(expectedOutput, Solution.main(input));
    }

    @Test
    public void testCase3() {
        String[] input = {"1\n" + "5 3\n" + "-1 3 0 2 -5\n"};
        String expectedOutput = "NO\n";
        Assert.assertEquals(expectedOutput, Solution.main(input));
    }
    @Test
    public void testCase4(){
        String[] input={"1\n" + "7 6\n" + "-1 0 4 2 1 7 9\n"};
        String expectedOutput="YES\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
    @Test
    public void testCase5(){
        String[] input={"1\n" + "10 9\n" + "0 -3 -4 -2 -5 -7 10 -3 -2 -1\n"};
        String expectedOutput="NO\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
    @Test
    public void testCase6(){
        String[] input={"1\n" + "8 4\n" + "-2 3 0 2 -5 3 5 6\n"};
        String expectedOutput="YES\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
    @Test
    public void badValues() {
        String[] input = {"6\n" + "4 3\n" + "-1 -3 4 2\n" + "4 2\n" + "0 -1 2 1\n" + "4 3\n" + "-1 -3 4 2\n"
                + "4 3\n" + "-1 -3 4 2\n" + "4 3\n" + "-1 -3 4 2\n" + "4 3\n" + "-1 -3 4 2\n"};
        String expectedOutput = "YES\n" + "NO\n" + "YES\n" + "YES\n" + "YES\n" + "YES\n";
        Assert.assertEquals(expectedOutput, Solution.main(input));
    }

}
