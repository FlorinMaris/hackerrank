package org.flo;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

class Solution {


    public static String main(String[] args){
        // setting up input and output
        ByteArrayInputStream in = new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here
        Scanner sc = new Scanner(System.in);
        int numberOfCases = sc.nextInt();
        try {
            validate(numberOfCases, 0, 5, "Incorrect number of cases");
        }catch (Exception e){
            e.printStackTrace();
        }
        int numberOfStudents = 0;
        int cancellationThreshold = 0;
        for (int i = 0; i < numberOfCases; i++) {
            numberOfStudents = sc.nextInt();
            try {
                validate(numberOfStudents, 3, 200, "Incorrect number of students");
            }catch (Exception e){
                e.printStackTrace();
            }
            cancellationThreshold = sc.nextInt();
            try {
                validate(cancellationThreshold, 1, numberOfStudents, "Incorrect cancellation limit");
            }catch (Exception e){
                e.printStackTrace();
            }
            int[] arrivalTime = new int[numberOfStudents];
            for (int j = 0; j < numberOfStudents; j++) {
                arrivalTime[j] = sc.nextInt();
                try {
                    validate(arrivalTime[j], -1000, 1000, "invalid arrival time entered");
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            printClassStatus(arrivalTime,cancellationThreshold);
        }
        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }

    public static void validate(int value, int lowerLimit, int upperLimit, String errorMessage) throws Exception {
        if (lowerLimit > value || value > upperLimit) throw new Exception(errorMessage);
    }
    public static void printClassStatus(int[] arrivalTime,int cancellationThreshold){
        int count=0;
        for (int time: arrivalTime) {
            if (time<=0) count++;
        }
        System.out.println((count<cancellationThreshold)?"YES":"NO");
    }

}