package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/30-binary-trees/problem

 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"6\n" + "3\n" + "5\n" + "4\n" + "7\n" + "2\n" + "1"};
        String expectedOutput="3 2 5 1 4 7 ";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
