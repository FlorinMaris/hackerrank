package org.flo;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

class Solution {

    public static String main(String[] args) {
        // setting up input and output
        ByteArrayInputStream in= new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out= new ByteArrayOutputStream();
        PrintStream printStream=new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here
        Scanner sc=new Scanner(System.in);
        int T=sc.nextInt();
        Node root=null;
        while(T-->0){
            int data=sc.nextInt();
            root=insert(root,data);
        }
        levelOrder(root);
        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }
    static void levelOrder(Node root){
        Node[] queue= new Node[1];
        queue[0]=root;
        while (queue!=null){
            System.out.print(queue[0].data+" ");
            if (queue[0].left!=null){
                queue=enqueue(queue,queue[0].left);
            }
            if (queue[0].right!=null){
                queue=enqueue(queue,queue[0].right);
            }
            queue=deque(queue);
        }
    }
    private static Node[] deque(Node[] queue){
        if(queue.length==1) return null;
        Node[] aux= new Node[queue.length-1];
        for (int i=0;i<aux.length;i++) {
            aux[i]=queue[i+1];
        }
        return aux;
    }
    private static Node[] enqueue(Node[] queue, Node node) {
        Node[] aux= new Node[queue.length+1];
        for (int i=0;i<queue.length;i++) {
            aux[i]=queue[i];
        }
        aux[aux.length-1]=node;
        return aux;
    }

    public static Node insert(Node root,int data){
        if(root==null){
            return new Node(data);
        }
        else{
            Node cur;
            if(data<=root.data){
                cur=insert(root.left,data);
                root.left=cur;
            }
            else{
                cur=insert(root.right,data);
                root.right=cur;
            }
            return root;
        }
    }
}