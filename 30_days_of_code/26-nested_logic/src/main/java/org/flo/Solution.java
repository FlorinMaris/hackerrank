package org.flo;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import java.time.LocalDate;
import java.util.*;

class Solution {


    public static String main(String[] args) {
        // setting up input and output
        ByteArrayInputStream in = new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here
        Scanner sc = new Scanner(System.in);
        Integer day = sc.nextInt();
        Integer month = sc.nextInt();
        Integer year = sc.nextInt();
        LocalDate actualReturnDate = LocalDate.of(year, month, day);
        day = sc.nextInt();
        month = sc.nextInt();
        year = sc.nextInt();
        LocalDate expectedReturnDate = LocalDate.of(year, month, day);
        System.out.println(fineValue(actualReturnDate,expectedReturnDate));
        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }

    public static int fineValue(LocalDate returnDate, LocalDate expectedDate) {
        if (returnDate.isBefore(expectedDate)) return 0;
        if (returnDate.getYear() - expectedDate.getYear() > 0) return 10000;
        if (returnDate.getMonthValue() - expectedDate.getMonthValue() > 0) {
            return 500 * (returnDate.getMonthValue() - expectedDate.getMonthValue());
        }
        return 15 * (returnDate.getDayOfMonth() - expectedDate.getDayOfMonth());

    }
}