package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/30-nested-logic/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"9 6 2015\n"+"6 6 2015"};
        String expectedOutput="45\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
    @Test
    public void testCaseOnTime(){
        String[] input={"9 6 2015\n"+"9 6 2015"};
        String expectedOutput="0\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
    @Test
    public void testCaseBeforeTime(){
        String[] input={"3 6 2015\n"+"9 6 2015"};
        String expectedOutput="0\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
    @Test
    public void testCaseMonth(){
        String[] input={"3 7 2015\n"+"9 6 2015"};
        String expectedOutput="500\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
    @Test
    public void testCaseYear(){
        String[] input={"3 7 2016\n"+"9 6 2015"};
        String expectedOutput="10000\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
