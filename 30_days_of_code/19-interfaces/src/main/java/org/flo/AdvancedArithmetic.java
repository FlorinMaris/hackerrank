package org.flo;

public interface AdvancedArithmetic {
    int divisorSum(int n);
}
