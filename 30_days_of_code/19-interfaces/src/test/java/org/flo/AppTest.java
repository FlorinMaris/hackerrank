package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/30-interfaces/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"6"};
        String expectedOutput="I implemented: org.flo.AdvancedArithmetic\n"+"12\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
