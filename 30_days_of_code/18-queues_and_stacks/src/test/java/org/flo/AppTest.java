package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/30-queues-stacks/problem

 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"racecar"};
        String expectedOutput="The word, racecar, is a palindrome.\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }

    @Test
    public void testCase2(){
        String[] input={"abcdef"};
        String expectedOutput="The word, abcdef, is not a palindrome.\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
