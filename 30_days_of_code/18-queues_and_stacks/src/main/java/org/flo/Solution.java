package org.flo;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

class Solution {
    private String stack="";
    private String queue="";

    public void pushCharacter(char c){
        this.stack=c+stack;
    }
    public void enqueueCharacter(char c){
        this.queue=queue+c;
    }
    public char popCharacter(){
        char c=stack.charAt(0);
        this.stack=stack.substring(1);
        return c;
    }
    public char dequeueCharacter(){
        char c=queue.charAt(0);
        this.queue=queue.substring(1);
        return c;
    }

    public static String main(String[] args) {
        // setting up input and output
        ByteArrayInputStream in= new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out= new ByteArrayOutputStream();
        PrintStream printStream=new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here
        Scanner scan = new Scanner(System.in);
        String input = scan.nextLine();
        scan.close();

        // Convert input String to an array of characters:
        char[] s = input.toCharArray();

        // Create a Solution object:
        Solution p = new Solution();

        // Enqueue/Push all chars to their respective data structures:
        for (char c : s) {
            p.pushCharacter(c);
            p.enqueueCharacter(c);
        }

        // Pop/Dequeue the chars at the head of both data structures and compare them:
        boolean isPalindrome = true;
        for (int i = 0; i < s.length/2; i++) {
            if (p.popCharacter() != p.dequeueCharacter()) {
                isPalindrome = false;
                break;
            }
        }

        //Finally, print whether string s is palindrome or not.
        System.out.println( "The word, " + input + ", is "
                + ( (!isPalindrome) ? "not a palindrome." : "a palindrome." ) );

        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }
}