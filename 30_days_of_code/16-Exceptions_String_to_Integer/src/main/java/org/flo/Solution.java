package org.flo;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

class Solution {


    public static String main(String[] args) {
        // setting up input and output
        ByteArrayInputStream in= new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out= new ByteArrayOutputStream();
        PrintStream printStream=new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here
        Scanner scanner = new Scanner(System.in);
        String S = scanner.next();
        try{
            System.out.print(Integer.parseInt(S));
        }catch(NumberFormatException badString){
            System.out.print("Bad String");
        }
        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }
}