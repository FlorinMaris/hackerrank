package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/30-exceptions-string-to-integer/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"3"};
        String expectedOutput="3";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }

    @Test
    public void testCase2(){
        String[] input={"z2"};
        String expectedOutput="Bad String";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
