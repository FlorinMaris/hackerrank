package org.flo;



import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;

/**
 The text of the problem can be found at the following link

 https://www.hackerrank.com/challenges/30-inheritance/problem

 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"Heraldo Memelli 8135627\n"+"2\n"+"100 80"};
        String expectedOutput="Name: Memelli, Heraldo\n"+"ID: 8135627\n"+"Grade: O\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
