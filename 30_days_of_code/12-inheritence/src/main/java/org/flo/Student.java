package org.flo;

class Student extends Person {
    private int[] testScores;

    Student(String firstName, String lastName, int id, int[] testScores) {
        super(firstName, lastName, id);
        this.testScores = testScores;
    }

    public char calculate() {
        double average;
        int sum = 0;
        if (testScores.length == 0) {
            return 'T';
        }
        for (int score : testScores) {
            sum += score;
        }
        average = sum / testScores.length;
        if (average < 40) {
            return 'T';
        }
        if (average < 55) {
            return 'D';
        }
        if (average < 70) {
            return 'P';
        }
        if (average < 80) {
            return 'A';
        }
        if (average < 90) {
            return 'E';
        }
        return 'O';
    }

}