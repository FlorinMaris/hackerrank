package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/30-linked-list-deletion/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"6\n" + "1\n" + "2\n" + "2\n" + "3\n" + "3\n" + "4"};
        String expectedOutput="1 2 3 4 ";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }@Test
    public void testCase2(){
        String[] input={"20\n3\n9\n9\n11\n11\n11\n11\n89\n89\n100\n100\n101\n102\n103\n108\n200\n250\n250\n250\n250"};
        String expectedOutput="3 9 11 89 100 101 102 103 108 200 250 ";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
