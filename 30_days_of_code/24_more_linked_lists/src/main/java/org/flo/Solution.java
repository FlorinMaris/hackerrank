package org.flo;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

class Solution {


    public static String main(String[] args) {
        // setting up input and output
        ByteArrayInputStream in= new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out= new ByteArrayOutputStream();
        PrintStream printStream=new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here
        Scanner sc=new Scanner(System.in);
        Node head=null;
        int T=sc.nextInt();
        while(T-->0){
            int ele=sc.nextInt();
            head=insert(head,ele);
        }
        head=removeDuplicates(head);
        display(head);
        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }
    public static Node removeDuplicates(Node head) {
        if(head==null){
            return null;
        }
        Node noDuplicatesHead= new Node(head.data);
        Node current=noDuplicatesHead;
        while (head.next!=null){
            while (head.data==head.next.data){
                head=head.next;
                if(head.next==null)break;
            }
            if(head.next==null)break;
            current.next=new Node(head.next.data);
            current=current.next;
            head=head.next;
        }
        return noDuplicatesHead;
    }
    public static  Node insert(Node head,int data)
    {
        Node p=new Node(data);
        if(head==null)
            head=p;
        else if(head.next==null)
            head.next=p;
        else
        {
            Node start=head;
            while(start.next!=null)
                start=start.next;
            start.next=p;

        }
        return head;
    }
    public static void display(Node head)
    {
        Node start=head;
        while(start!=null)
        {
            System.out.print(start.data+" ");
            start=start.next;
        }
    }
}