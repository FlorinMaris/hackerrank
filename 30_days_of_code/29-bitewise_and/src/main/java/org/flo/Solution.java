package org.flo;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

class Solution {


    public static String main(String[] args) {
        // setting up input and output
        ByteArrayInputStream in= new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out= new ByteArrayOutputStream();
        PrintStream printStream=new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here
        Scanner sc=new Scanner(System.in);
        int numberOfTestCases =sc.nextInt();
        int[] results=new int[numberOfTestCases];
        for (int i=0;i<numberOfTestCases;i++){
            int n = sc.nextInt();
            int k = sc.nextInt();
            for (int a=1;a<n;a++){
                for (int b=a+1;b<=n;b++){
                    if((a&b)>results[i] && (a&b)<k){
                        results[i]=a&b;
                    }
                }
            }
        }
        for (int i=0;i<numberOfTestCases;i++){
            System.out.println(results[i]);
        }
        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }
}