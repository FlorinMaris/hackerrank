package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/30-bitwise-and/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"3\n" +
                "5 2\n" +
                "8 5\n" +
                "2 2"};
        String expectedOutput="1\n" + "4\n" + "0\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
