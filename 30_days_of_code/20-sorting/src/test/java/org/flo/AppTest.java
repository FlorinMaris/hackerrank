package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/30-sorting/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"3\n" + "1 2 3"};
        String expectedOutput="Array is sorted in 0 swaps.\n" + "First Element: 1\n" + "Last Element: 3";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }

    @Test
    public void testCase2(){
        String[] input={"3\n" + "3 2 1"};
        String expectedOutput="Array is sorted in 3 swaps.\n" + "First Element: 1\n" + "Last Element: 3";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }

    @Test
    public void testCase3(){
        String[] input={"2\n" + "3 2"};
        String expectedOutput="Array is sorted in 1 swaps.\n" + "First Element: 2\n" + "Last Element: 3";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
    @Test
    public void testCase4(){
        String[] input={"2\n" + "3 3"};
        String expectedOutput="Array is sorted in 0 swaps.\n" + "First Element: 3\n" + "Last Element: 3";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
    @Test
    public void testCase5(){
        String[] input={"6\n" + "10 3 2 4 5 2"};
        String expectedOutput="Array is sorted in 9 swaps.\n" + "First Element: 2\n" + "Last Element: 10";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
