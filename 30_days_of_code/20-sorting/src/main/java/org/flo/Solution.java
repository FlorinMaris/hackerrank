package org.flo;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

class Solution {


    public static String main(String[] args) {
        // setting up input and output
        ByteArrayInputStream in = new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        for (int a_i = 0; a_i < n; a_i++) {
            a[a_i] = scanner.nextInt();
        }
        boolean isSorted;
        int totalSwaps = 0;
        for (int i = 1; i < n; i++) {
            isSorted = true;
            for (int j = 0; j < n - i; j++) {
                if (a[j] > a[j + 1]) {
                    switchValues(a, j, j + 1);
                    totalSwaps++;
                    isSorted = false;
                }

            }
            if (isSorted) {
                break;
            }
        }
        System.out.print("Array is sorted in " + totalSwaps + " swaps.\n" + "First Element: " + a[0] + "\n" + "Last Element: " + a[n - 1]);
        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }

    public static void switchValues(int[] array, int firstIndex, int secondIndex) {
        int aux = array[firstIndex];
        array[firstIndex] = array[secondIndex];
        array[secondIndex] = aux;
    }


}