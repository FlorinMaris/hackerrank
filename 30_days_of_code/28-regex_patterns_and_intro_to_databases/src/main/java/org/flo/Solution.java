package org.flo;


import javax.jws.soap.SOAPBinding;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Solution {
    static class User implements Comparable<User>{
        private String name;
        private String email;
        User(String name,String email){
            this.name=name;
            this.email=email;
        }

        public String getEmail() {
            return email;
        }

        public String getName() {
            return name;
        }
        public int compareTo(User user) {
            return (this.getName()+this.getEmail()).compareTo(user.getName()+user.getEmail());
        }
    }

    public static String main(String[] args) {
        // setting up input and output
        ByteArrayInputStream in= new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out= new ByteArrayOutputStream();
        PrintStream printStream=new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here
        Scanner sc= new Scanner(System.in);
        int numberOfUsers=sc.nextInt();
        Set<User> users = new TreeSet<User>();
               for (int i=0;i<numberOfUsers;i++){
        users.add(new User(sc.next(),sc.next()));
        }
        for (User user:users){
            if(user.getEmail().matches(".*@gmail\\.com$")){
                System.out.println(user.getName());
            }
        }
        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }

}