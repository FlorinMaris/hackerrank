package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/30-regex-patterns/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"6\n" +
                "riya riya@gmail.com\n" +
                "julia julia@julia.me\n" +
                "julia sjulia@gmail.com\n" +
                "julia julia@gmail.com\n" +
                "samantha samantha@gmail.com\n" +
                "tanya tanya@gmail.com"};
        String expectedOutput="julia\n" +
                "julia\n" +
                "riya\n" +
                "samantha\n" +
                "tanya\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
