package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/30-generics/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"3\n 1 2 3\n 2\nfirst second\n"};
        String expectedOutput="1\n2\n3\nfirst\nsecond\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
    @Test
    public void testNumberOfMethods(){
        Assert.assertEquals(1,Printer.class.getDeclaredMethods().length);
    }
}
