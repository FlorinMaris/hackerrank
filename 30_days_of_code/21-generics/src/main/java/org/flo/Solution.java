package org.flo;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

class Solution {


    public static String main(String[] args) {
        // setting up input and output
        ByteArrayInputStream in= new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out= new ByteArrayOutputStream();
        PrintStream printStream=new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        Integer[] intArray = new Integer[n];
        for (int i = 0; i < n; i++) {
            intArray[i] = scanner.nextInt();
        }

        n = scanner.nextInt();
        String[] stringArray = new String[n];
        for (int i = 0; i < n; i++) {
            stringArray[i] = scanner.next();
        }

        Printer<Integer> intPrinter = new Printer<Integer>();
        Printer<String> stringPrinter = new Printer<String>();
        intPrinter.printArray( intArray  );
        stringPrinter.printArray( stringArray );
        if(Printer.class.getDeclaredMethods().length > 1){
            System.out.println("The Printer class should only have 1 method named printArray.");
        }
        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }
}