package org.flo;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

class Solution {

    public static  Node insert(Node head,int data) {
        if(head==null){
            head=new Node(data);
            return head;
        }
        Node start=head;
        while(start.next != null){
            start=start.next;
        }
        start.next= new Node(data);

        return head;
    }
    public static void display(Node head) {
        Node start = head;
        while(start != null) {
            System.out.print(start.data + " ");
            start = start.next;
        }
    }

    public static String main(String[] args) {
        // setting up input and output
        ByteArrayInputStream in= new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out= new ByteArrayOutputStream();
        PrintStream printStream=new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here
        Scanner sc = new Scanner(System.in);
        Node head = null;
        int N = sc.nextInt();
        while(N-- > 0) {
            int ele = sc.nextInt();
            head = insert(head,ele);
        }
        display(head);
        sc.close();
        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }
}