package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/30-linked-list/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"4\n" +"2\n" +"3\n" +"4\n" +"1"};
        String expectedOutput="2 3 4 1 ";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
