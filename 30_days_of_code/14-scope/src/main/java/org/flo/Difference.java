package org.flo;

    class Difference {
        private int[] elements;
        public int maximumDifference;
    Difference(int[] elements){
        this.elements=elements;
    }
    public int computeDifference(){
        for (int number1:elements) {
            for (int number2:elements) {
                int difference=Math.abs(number1-number2);
                if(difference>maximumDifference){
                    maximumDifference=difference;
                }
            }
        }
        return maximumDifference;
    }

}
