package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/30-scope/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"3\n" + "1 2 5"};
        String expectedOutput="4";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
