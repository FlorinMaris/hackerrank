package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/30-more-exceptions/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"4\n"+"3 5\n"+"2 4\n"+"-1 -2\n"+"-1 3"};
        String expectedOutput="243\n" +"16\n" +"n and p should be non-negative\n" +"n and p should be non-negative\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
