package org.flo;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

class Solution {


    public static String main(String[] args) {
        // setting up input and output
        ByteArrayInputStream in= new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out= new ByteArrayOutputStream();
        PrintStream printStream=new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here
        Scanner sc=new Scanner(System.in);
        int T=sc.nextInt();
        Node root=null;
        while(T-->0){
            int data=sc.nextInt();
            root=insert(root,data);
        }

        int height=getHeight(root);
        System.out.println(height);

        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }
    public static int getHeight(Node root){
        if (isLeaf(root)) return 0;
        if (root.right==null) {
            return 1 + getHeight(root.left);
        }
        if (root.left==null){
            return 1+getHeight(root.right);
        }
        return 1+Math.max(getHeight(root.left),getHeight(root.right));
    }
    public static boolean isLeaf(Node node){
        return (node.left==null && node.right==null);
    }
    public static Node insert(Node root,int data){
        if(root==null){
            return new Node(data);
        }
        else{
            Node cur;
            if(data<=root.data){
                cur=insert(root.left,data);
                root.left=cur;
            }
            else{
                cur=insert(root.right,data);
                root.right=cur;
            }
            return root;
        }
    }
}