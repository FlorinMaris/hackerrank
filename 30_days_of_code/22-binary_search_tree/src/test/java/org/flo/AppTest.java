package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/30-binary-search-trees/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"7\n" +"3\n" + "5\n" + "2\n" + "1\n" + "4\n" + "6\n" + "7\n"};
        String expectedOutput="3\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
