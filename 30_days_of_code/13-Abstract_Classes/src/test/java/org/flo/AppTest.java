package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/30-abstract-classes/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"The Alchemist\n" + "Paulo Coelho\n" + "248"};
        String expectedOutput="Title: The Alchemist\n" + "Author: Paulo Coelho\n" + "Price: 248";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
