

package org.flo;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
public class SolutionTest {
    @Test
    public void testMatrix1() {
        Solution solution = new Solution();
        int[][] array = {{1, 1, 1, 0, 0, 0},
                {0, 1, 0, 0, 0, 0},
                {1, 1, 1, 0, 0, 0},
                {0, 0, 2, 4, 4, 0},
                {0, 0, 0, 2, 0, 0},
                {0, 0, 1, 2, 4, 0}};
        solution.setArr(array);
        assertEquals(19,solution.getMaxSum());
    }
    @Test
    public void testMatrix2() {
        Solution solution = new Solution();
        int[][] array = {{-1, -1, 0, -9, -2, -2},
                {-2, -1, -6, -8, -2, -5},
                {-1, -1, -1, -2, -3, -4},
                {-1, -9, -2, -4, -4, -5},
                {-7, -3, -3, -2 -9 -9},
                {-1 -3 -1 -2 -4 -5}};
        solution.setArr(array);
        assertEquals(-6,solution.getMaxSum());
    }
}
