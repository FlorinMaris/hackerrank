package org.flo;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    private int arr[][];

    public void setArr(int[][] arr) {
        this.arr = arr;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Solution solution=new Solution();
        int arr[][] = new int[6][6];
        for(int i=0; i < 6; i++){
            for(int j=0; j < 6; j++){
                arr[i][j] = in.nextInt();
            }
        }
        int maxSum = solution.getMaxSum();
        System.out.println(maxSum);
    }

    public int getMaxSum() {
        int maxSum=Integer.MIN_VALUE;
        for (int i=1;i<5;i++){
            for(int j=1;j<5;j++){
                maxSum=maxSum<hourglassSum(i,j,arr)?hourglassSum(i,j,arr):maxSum;
            }
        }
        return maxSum;
    }

    public int hourglassSum(int i,int j, int[][] array){
        int sumTop=array[i-1][j-1]+array[i-1][j]+array[i-1][j+1];
        int sumMiddle=array[i][j];
        int sumBottom=array[i+1][j-1]+array[i+1][j]+array[i+1][j+1];
        return sumTop+sumMiddle+sumBottom;
    }

}
