package org.flo;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

class Solution {


    public static String main(String[] args) {
        // setting up input and output
        ByteArrayInputStream in= new ByteArrayInputStream(args[0].getBytes());
        ByteArrayOutputStream out= new ByteArrayOutputStream();
        PrintStream printStream=new PrintStream(out);
        System.setIn(in);
        System.setOut(printStream);
        // code from HackerRank goes here
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int[] number=new int[n];
        for (int i=0;i<n;i++){
        number[i]=sc.nextInt();
        }
        for (int i=0;i<n;i++){
            System.out.println(isPrime(number[i])?"Prime":"Not prime");
        }

        // returning output in order to apply unit tests
        printStream.close();
        return out.toString();
    }
    public static boolean isPrime(int n){
        if(n==2 || n==3 ) return true;
        if(n%2==0 || n==0 || n==1) return false;
        boolean result=true;
        for(int i=3;i<=n/2;i+=2){
            if (n%i==0) return false;
        }
        return result;
    }
}