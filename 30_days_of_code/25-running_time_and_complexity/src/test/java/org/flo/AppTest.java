package org.flo;


import org.junit.Assert;
import org.junit.Test;


/**
 The text of the task can be found at the following link

 https://www.hackerrank.com/challenges/30-running-time-and-complexity/problem
 */
public class AppTest
{
    @Test
    public void testCase1(){
        String[] input={"3\n" + "12\n" + "5\n" + "7"};
        String expectedOutput="Not prime\n" + "Prime\n" + "Prime\n";
        Assert.assertEquals(expectedOutput,Solution.main(input));
    }
}
